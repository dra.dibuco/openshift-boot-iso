#!/bin/bash

curl -o rhcos-4.5.6-x86_64-installer.x86_64.iso https://mirror.openshift.com/pub/openshift-v4/dependencies/rhcos/4.5/4.5.6/rhcos-4.5.6-x86_64-installer.x86_64.iso

mkdir /mnt/iso_rhcos

mount rhcos-4.5.6-x86_64-installer.x86_64.iso /mnt/iso_rhcos

mkdir /tmp/rhcos

rsync -a /mnt/iso_rhcos/* /tmp/rhcos/

cd /tmp/rhcos

chmod +w isolinux/isolinux.cfg

cp -rf /src/isolinux.cfg isolinux/isolinux.cfg

chmod -w isolinux/isolinux.cfg

cd /tmp/rhcos

mkisofs -U -A "RHCOS-x86_64" -V "RHCOS-x86_64" -volset "RHCOS-x86_64" -J -joliet-long -r -v -T -x ./lost+found \
-o /src/rhcos_install_modified.iso -b isolinux/isolinux.bin -c isolinux/boot.cat \
-no-emul-boot -boot-load-size 4 -boot-info-table -eltorito-alt-boot -e images/efiboot.img -no-emul-boot .

isohybrid /src/rhcos_install_modified.iso
